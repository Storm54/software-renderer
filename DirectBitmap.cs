﻿//FROM: http://stackoverflow.com/questions/24701703/c-sharp-faster-alternatives-to-setpixel-and-getpixel-for-bitmaps-for-windows-f

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace SoftwareRenderer
{
    public class DirectBitmap : IDisposable
    {
        public readonly Bitmap bitmap;

        public readonly uint[] bits;

        public bool Disposed { get; private set; }

        public readonly int height;
        public readonly int width;

        private GCHandle bitsHandle;

        public DirectBitmap(int width, int height)
        {
            this.width = width;
            this.height = height;
            bits = new uint[width * height];
            bitsHandle = GCHandle.Alloc(bits, GCHandleType.Pinned);
            bitmap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppPArgb, bitsHandle.AddrOfPinnedObject());
        }

        public void Dispose()
        {
            if (Disposed) return;
            Disposed = true;
            bitmap.Dispose();
            bitsHandle.Free();
        }
    }
}