﻿namespace SoftwareRenderer
{
    public partial class Renderer
    {
        private void Clear(uint color)
        {
            for (var i = 0; i < directBitmap.bits.Length; i++) directBitmap.bits[i] = color;
        }

        private void Pixel(int x, int y, uint color)
        {
            if (x < 0 || x >= width) return;
            if (y < 0 || y >= height) return;

            directBitmap.bits[y * width + x] = color;
        }

        private void Line(int x0, int y0, int x1, int y1, uint color)
        {
            var dx = (x1 > x0) ? (x1 - x0) : (x0 - x1);
            var dy = (y1 > y0) ? (y1 - y0) : (y0 - y1);

            var sx = (x1 >= x0) ? (1) : (-1);
            var sy = (y1 >= y0) ? (1) : (-1);

            if (dy < dx)
            {
                var d = (dy << 1) - dx;
                var d1 = dy << 1;
                var d2 = (dy - dx) << 1;
                Pixel(x0, y0, color);
                var x = x0 + sx;
                var y = y0;
                for (var i = 1; i <= dx; i++)
                {
                    if (d > 0)
                    {
                        d += d2;
                        y += sy;
                    }
                    else
                        d += d1;
                    Pixel(x, y, color);
                    x++;
                }
            }
            else
            {
                var d = (dx << 1) - dy;
                var d1 = dx << 1;
                var d2 = (dx - dy) << 1;
                Pixel(x0, y0, color);
                var x = x0;
                var y = y0 + sy;
                for (var i = 1; i <= dy; i++)
                {
                    if (d > 0)
                    {
                        d += d2;
                        x += sx;
                    }
                    else
                        d += d1;
                    Pixel(x, y, color);
                    y++;
                }
            }
        }
    }
}