﻿using System;
using System.Diagnostics;
using System.Drawing;

namespace SoftwareRenderer
{
    public partial class Renderer : IDisposable
    {
        public readonly ushort width;
        public readonly ushort height;

        private readonly DirectBitmap directBitmap;

        public double DeltaTime { get { return deltaTime; } }
        private double deltaTime;

        private readonly Stopwatch stopwatch = new Stopwatch();

        public Renderer(ushort width, ushort height)
        {
            this.width = width;
            this.height = height;

            directBitmap = new DirectBitmap(width, height);
        }

        public void Draw(Graphics graphics)
        {
            stopwatch.Start();

            Render();

            deltaTime = stopwatch.Elapsed.TotalMilliseconds;

            stopwatch.Reset();

            graphics.DrawImage(directBitmap.bitmap, 0, 0);
        }

        public void Dispose()
        {
            directBitmap.Dispose();
        }

        public void Render()
        {
            //A R G B
            uint b = 0xFF000050;

            Clear(b);

            Line(100, 100, 400, 600, 0xFFFFFFFF);
        }
    }
}