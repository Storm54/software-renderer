﻿using System.Drawing;
using System.Windows.Forms;

namespace SoftwareRenderer
{
    public partial class Window : Form
    {
        private const int ScreenWidth = 1024;
        private const int ScreenHeight = 768;

        private Renderer renderer;

        public Window()
        {
            InitializeComponent();

            CreateBitmap();
        }

        private void CreateBitmap()
        {
            ClientSize = new Size(ScreenWidth, ScreenHeight);

            renderer = new Renderer(ScreenWidth, ScreenHeight);
        }

        private void OnClose(object sender, FormClosedEventArgs e)
        {
            renderer.Dispose();
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            renderer.Render();

            renderer.Draw(e.Graphics);

            var fps = renderer.DeltaTime.Equals(0f) ? double.PositiveInfinity : (int)(1d / renderer.DeltaTime * 1000d);

            e.Graphics.DrawString("FPS: " + fps, DefaultFont, new SolidBrush(Color.White), 10, 10); Invalidate();
        }
    }
}